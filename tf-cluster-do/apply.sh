# LOAD ENVIROMENT VARIABLES

dotenv() {
    set -a
    [ -f .env ] && . .env
    set +a
}

dotenv

# EXECUTE TERRAFORM APPLY

terraform apply
