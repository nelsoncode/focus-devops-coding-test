# SAVE STATE REMOTE IN GITLAB

# DOC REFERENCE -> https://docs.gitlab.com/ee/user/infrastructure/iac/terraform_state.html

# LOAD ENVIROMENT VARIABLES

dotenv() {
    set -a
    [ -f .env ] && . .env
    set +a
}

dotenv

PROJECT_ID=$PROJECT_ID
STATE_NAME=$STATE_NAME
TF_USERNAME=$TF_USERNAME
TF_PASSWORD=$TF_PASSWORD
TF_ADDRESS="https://gitlab.com/api/v4/projects/${PROJECT_ID}/terraform/state/${STATE_NAME}"

terraform init \
    -backend-config=address=${TF_ADDRESS} \
    -backend-config=lock_address=${TF_ADDRESS}/lock \
    -backend-config=unlock_address=${TF_ADDRESS}/lock \
    -backend-config=username=${TF_USERNAME} \
    -backend-config=password=${TF_PASSWORD} \
    -backend-config=lock_method=POST \
    -backend-config=unlock_method=DELETE \
    -backend-config=retry_wait_min=5
