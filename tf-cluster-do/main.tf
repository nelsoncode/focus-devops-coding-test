resource "digitalocean_kubernetes_cluster" "my_cluster" {
  name         = "my-cluster"
  region       = var.REGION
  version      = var.VERSION_KUBERNETES_CLUSTER
  auto_upgrade = true

  node_pool {
    name       = var.NAME_NODE_POOL
    size       = var.SIZE_NODE_POOL
    node_count = var.SIZE_NODE_COUNT
    auto_scale = true
    min_nodes  = 1
    max_nodes  = 3
  }
}

provider "kubernetes" {
  host                   = digitalocean_kubernetes_cluster.my_cluster.endpoint
  token                  = digitalocean_kubernetes_cluster.my_cluster.kube_config[0].token
  cluster_ca_certificate = base64decode(digitalocean_kubernetes_cluster.my_cluster.kube_config[0].cluster_ca_certificate)
}
