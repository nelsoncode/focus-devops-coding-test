variable "TOKEN_SECRET" {
  type        = string
  description = "TOKEN SECRET of Digital Ocean"
  sensitive   = true
}

# KUBERNETES'S VARIABLES

variable "NAME_CLUSTER" {
  type    = string
  default = "my-cluster"
}


variable "REGION" {
  type        = string
  description = "Digital Ocean's region"
  default     = "sfo3"
}

variable "VERSION_KUBERNETES_CLUSTER" {
  type        = string
  description = "Cluster version"
  default     = "1.21"
}

variable "SIZE_NODE_POOL" {
  type        = string
  description = "Size node pool"
  default     = "s-2vcpu-2gb"
}

variable "SIZE_NODE_COUNT" {
  type        = number
  description = "Size node cluster"
  default     = 3
}
variable "NAME_NODE_POOL" {
  type    = string
  default = "k8s-node-pool"
}
