# LOAD ENVIROMENT VARIABLES

dotenv() {
    set -a
    [ -f .env ] && . .env
    set +a
}

dotenv

# EXECUTE TERRAFORM DESTROY

terraform destroy
